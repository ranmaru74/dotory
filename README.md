# Dotory Hackathon Project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```


### Compiles and minifies for production
### dist folder is the output location of build sources
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



## visual studio code terminal commands

### Firebase
```
firebase login
firebase init
firebase public directory ===> dist
firebase deploy
```

### Git
```
git clone https://github.com/hahahahao/dotory.git   in github
  -> or git clone https://babolulu3333@bitbucket.org/ranmaru74/dotory.git    in bitbucket
git add *.* : make indexs of all files and folders 
git commit -m "write comments here blablablabla"
git remote add origin https://github.com/hahahahao/dotory.git   in github
  -> or git remote add origin https://babolulu3333@bitbucket.org/ranmaru74/dotory.git    in bitbucket
git push -u origin "master" // or branch name to push

Git Branch Related Commands
git remote update
git branch -a // to check all branchs
git branch // to check your current branch
git fetch origin
git checkout 'branch name' //  to change current branch
```

You can use data in `data/` folder to import to `products` node in your firebase app. 
So it is nessary to create 'products' node in 'Realtime Database of Firebase' before the data insertion.

## Features

This project implements basic indie investment cart features:
* Login / Register
* Pull products list from API
* Add/Remove item to indie investment cart

## Technical details

* VueJS [^2.2.1]
* [Bootstrap 4](https://getbootstrap.com/)
* Firebase (auth and realtime database)

## Demo

Checkout demo at [Demo](http://mydb-d09a2.firebaseapp.com)


## Contributing

As I use this for Hedera Hackathon projects, I know this might not be the perfect approach
for all the projects out there. If you have any ideas, just
[open an issue][issues] and tell me what you think.

If you'd like to contribute, please fork the repository and make changes as
you'd like. Pull requests are warmly welcome.