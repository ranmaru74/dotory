import { EventBus } from "@/eventBus";
import store from "@/stores/store";

export function notifySuccess(message) {
    showMessage(message, "success");
}

export function notifyError(message) {
    showMessage(message, "danger");
}

function showMessage(message, messageClass) {
    const message_obj = {
        message,
        messageClass,
        autoClose: true
    }
    store.dispatch('addMessage', message_obj);
}

// function notify(status, message) {
//     EventBus.$emit("notify", {
//         status,
//         message
//     })
// }

export function getPrivateKeyForAccount(accountId) {
    return store.getters.getAccounts[accountId].privateKey
}


// export function getAccountDetails(account) {
//     for(const key in store.getters.getAccounts) {
//         if(store.getters.getAccounts[key].account.wallet)
//     }
// }