import Store from './components/Store.vue';
import ProductDetails from './components/ProductDetails.vue';
import Login from './components/auth/Login.vue';
import Register from './components/auth/Register.vue';
import Account from "@/components/account/Account";
import Admin from "@/components/account/Admin";
import RegisterSinger from "@/components/auth/RegisterSinger";
import SingerDetails from "@/components/SingerDetails";

export const routes = [
    { path: '/', component: Store, name: 'mainpage', onlyGuest: true },
    { path: '/admin', component: Admin, name: 'admin'},
    { path: '/account', component: Account, name: 'account'},
    { path: '/product/:id', component: ProductDetails, name: 'product' },
    { path: '/login', component: Login, name: 'login', onlyGuest: true },
    { path: '/register', component: Register, name: 'register', onlyGuest: true },
    { path: '/register-singer', component: RegisterSinger, name: 'registerSinger' },
    { path: '/singer-details', component: SingerDetails, name: 'singerDetails' },
    { path: '*', redirect: '/' }
];