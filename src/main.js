import Vue from 'vue';
import VueRouter from 'vue-router';

import { routes } from './routes';
import store from './stores/store';
import { ref, firebaseListener } from './config/firebaseConfig';
import './assets/styles/app.scss'

require("dotenv").config();
import App from './App.vue';
import { fetchTokenFromFirebase } from "@/service/firebaseService";

Vue.use(VueRouter);


firebaseListener(authStatusChange);


const router = new VueRouter({
    mode: 'history',
    routes
});

// router.beforeEach((to, from, next) => {
//     if (to.onlyGuest && store.getters.isLoggedIn) {
//         next('/');
//     } else {
//         next();
//     }
// });


new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
})

function authStatusChange(loggedIn, user) {
    if (store) {
        store.commit('AUTH_STATUS_CHANGE');
        console.log('AUTH_STATUS_CHANGE');
        console.log('isLoggedIn : ' + store.getters.isLoggedIn);
        console.log(user);
        if (user) {
            ref.child('account').once('value').then((accounts) => {
                const data = accounts.val();
                const email = user.email;
                store.commit('setAllAccountsAndCurrentAccount', { data, email });
            });
            ref.child('admin').once('value').then((admin) => {
                const data = admin.val();
                console.log('get admin from firebase');
                console.log(data);
                store.dispatch('fetchAdmin', data)
                // store.commit('setAdmin', admin);
            });
            fetchTokenFromFirebase();
            // store.dispatch('getShoppingCart', { uid: user.uid, currentCart: store.getters.cartItemList });
        } else {
            store.commit('clearHederaAccount');
        }
    }
}
