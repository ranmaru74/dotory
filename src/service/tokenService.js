import {
    TokenCreateTransaction,
    PrivateKey,
    TokenInfoQuery,
    Hbar,
    Status, TokenAssociateTransaction, TransferTransaction, TokenGrantKycTransaction
} from "@hashgraph/sdk";
import { getClient, operatorClient } from "@/service/client";
import { getPrivateKeyForAccount, notifyError, notifySuccess } from "@/utils";
import store from "@/stores/store";

export async function tokenCreate(account, token) {
    let tokenResponse = {}
    const autoRenewPeriod = 7776000;
    try {
        console.log('tokenService tokenCreate call');
        console.log(token);
        let sigKey = PrivateKey.fromString(token.key);
        const tx = await new TokenCreateTransaction();
        tx.setTokenName(token.name);
        tx.setTokenSymbol(token.symbol.toUpperCase());
        tx.setDecimals(token.decimals);
        tx.setInitialSupply(token.initialSupply);
        tx.setTreasuryAccountId(token.treasury);
        tx.setAutoRenewAccountId(token.autoRenewAccount);
        tx.setAutoRenewPeriod(autoRenewPeriod);

        tx.setAdminKey(sigKey.publicKey);
        tx.setKycKey(sigKey.publicKey);
        tx.setFreezeKey(sigKey.publicKey);
        tx.setFreezeDefault(false);
        tx.setWipeKey(sigKey.publicKey);
        tx.setSupplyKey(sigKey.publicKey);

        console.log('tokenCreate account');
        console.log(account.accountId);
        console.log(account.privateKey);

        const client = getClient(account.accountId, account.privateKey);

        await tx.signWithOperator(client);
        await tx.sign(sigKey);


        console.log('1');
        const response = await tx.execute(client);
        console.log('2');

        const transactionReceipt = await response.getReceipt(client);

        if (transactionReceipt.status !== Status.Success) {
            notifyError(transactionReceipt.status.toString());
        } else {
            token.tokenId = transactionReceipt.tokenId;

            // Todo: make transaction

            const tokenInfo = await tokenGetInfo(token);

            tokenResponse = {
                tokenId: token.tokenId.toString(),
                symbol: token.symbol.toUpperCase(),
                name: token.name,
                totalSupply: token.initialSupply,
                decimals: token.decimals,
                autoRenewAccount: account.accountId.toString(),
                autoRenewPeriod: autoRenewPeriod,
                defaultFreezeStatus: token.defaultFreezeStatus,
                kycKey: token.kycKey,
                wipeKey: token.wipeKey,
                freezeKey: token.freezeKey,
                adminKey: token.adminKey,
                supplyKey: token.supplyKey,
                expiry: tokenInfo.expiry,
                isDeleted: false,
                treasury: account.accountId.toString()
            }

            notifySuccess("token create successful");
        }
        return tokenResponse;
    } catch (err) {
        notifyError(err.message);
        return {};
    }
}

export async function tokenGetInfo(token) {
    const client = operatorClient();
    const tokenResponse = token;
    try {
        const info = await new TokenInfoQuery()
            .setQueryPayment(Hbar.fromTinybars(30))
            .setTokenId(token.tokenId)
            .execute(client)

        tokenResponse.totalSupply = info.totalSupply;
        tokenResponse.expiry = info.expirationTime.toDate();
    } catch (err) {
        notifyError(err.message);
    }

    return tokenResponse;
}

export async function tokenAssociate(tokenId, account) {
    const tx = await new TokenAssociateTransaction();

    const client = getClient(account.accountId, account.privateKey);

    const userKey = PrivateKey.fromString(account.privateKey);

    try {
        tx.setTokenIds([tokenId]);
        tx.setAccountId(account.accountId);

        await tx.signWithOperator(client);
        await tx.sign(userKey);

        const response = await tx.execute(client);

        const transactionReceipt = await response.getReceipt(client);
        if(transactionReceipt.status !== Status.Success) {
            console.log("token association status is not success");
            console.log(transactionReceipt.status.toString());
            notifyError(transactionReceipt.status.toString());
            return {
                status: false
            }
        }

        notifySuccess("token association successful");
        return {
            status: true,
            transactionId: response.transactionId.toString()
        };
    } catch (err) {
        notifyError(err.message);
        return {
            status: false
        };
    }
}

export async function tokenGrantKYC(instruction) {
    const token = store.getters.getAdminToken;
    console.log('get token');
    console.log(token);
    const kycKey = PrivateKey.fromString(token.kycKey);
    const tx = await new TokenGrantKycTransaction();
    instruction.successMessage =
        "Account" + instruction.accountId + " KYC Granted";
    const admin = store.getters.getAdmin;
    const adminClient = getClient(admin.accountId, admin.privateKey);
    const result = await tokenTransactionWithIdAndAccount(adminClient, tx, instruction, kycKey);

    if(result.status) {
        const transaction = {
            id: result.transactionId,
            type: "tokenGrantKYC",
            inputs:
                "tokenId=" +
                instruction.tokenId +
                ", AccountId=" +
                instruction.accountId
        };
        console.log('tokenGrantKYC transaction success');
        console.log(transaction)
    }

    return result.status;
}

export async function tokenTransfer(tokenId, account, quantity, hbar, destination) {
    const client = getClient(account.accountId, account.privateKey);
    try {
        const tx = await new TransferTransaction();
        tx.addTokenTransfer(tokenId, destination.accountId, quantity);
        tx.addTokenTransfer(tokenId, account.accountId, -quantity);
        if(hbar !== 0) {
            tx.addHbarTransfer(destination, new Hbar(-hbar));
            tx.addHbarTransfer(account.accountId, new Hbar(hbar));
            tx.freezeWith(client);
            const sigKey = await PrivateKey.fromString(getPrivateKeyForAccount(destination));
            await tx.sign(sigKey);
        }
        const result = await tx.execute(client);
        const transactionReceipt = await result.getReceipt(client);

        if (transactionReceipt.status !== Status.Success) {
            notifyError(transactionReceipt.status.toString());
            return false;
        } else {
            notifySuccess("token transfer successful");
            return true;
        }
    } catch(err) {
        notifyError(err.message);
        return false;
    }

}

async function tokenTransactionWithIdAndAccount(client, transaction, instruction, key) {
    try {
        console.log('tokenTransactionWithIdAndAccount instruction');
        console.log(instruction);
        transaction.setTokenId(instruction.tokenId);
        transaction.setAccountId(instruction.accountId);

        await transaction.signWithOperator(client);
        await transaction.sign(key);

        const response = await transaction.execute(client);

        const transactionReceipt = await response.getReceipt(client);
        if(transactionReceipt.status != Status.Success) {
            notifyError(transactionReceipt.status.toString());
            return {
                status: false
            }
        }

        notifySuccess(instruction.successMessage);
        return {
            status: true,
            transactionId: response.transactionId.toString()
        }

    } catch(err) {
        notifyError(err.message);
        return {
            status: false
        }
    }
}