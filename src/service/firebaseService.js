import { ref, firebaseAuth } from "@/config/firebaseConfig";
import store from '@/stores/store';
import { EventBus } from "@/eventBus";

export const saveAdminToFirebase = (account) => {
    return ref.child('admin').set(account)
}

export const saveTokenToFirebase = (token) => {
    return ref.child('token').set(token);
}

export const fetchTokenFromFirebase = () => {
    ref.child('token').once('value')
        .then(response => {
            console.log('fetchTokenFromFirebase');
            const token = response.val();
            console.log(token);
            store.commit('setToken', token);
            EventBus.$emit('updateTokenStatus', true);
        })
}

export async function addSinger(singer) {
    console.log(singer);
    return ref.child('products').push().set(singer)
        // .then(response => {
        //     console.log('addSinger result');
        //     console.log(response);
        // });
}
