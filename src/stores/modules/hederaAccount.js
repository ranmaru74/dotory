import Vue from 'vue';
import { ref } from "@/config/firebaseConfig";

const state = {
    admin: {},
    account: {},
    accounts: {},
    token: {},
    tokens: {},
    allTokens: {}
}

const getters = {
    numberOfAccounts(state) {
        return Object.keys(state.accounts).length || 0;
    },
    getAdmin(state) {
        return state.admin;
    },
    getAccount(state) {
        return state.account;
    },
    getAccounts(state) {
        return state.accounts;
    },
    getCurrentAccount(state) {
        return state.account;
    },
    getToken(state) {
        return state.tokens;
    },
    getAdminToken(state) {
        return state.token;
    }
}

const mutations = {
    setAdmin(state, account) {
        state.admin = account;
    },
    setCurrentAccount(state, account) {
        state.account = account
    },
    setAccount(state, account) {
        // state.accounts[account.accountId] = account
        Vue.set(state.accounts, account.accountId, account);
    },
    // setToken(state, token) {
    //     Vue.set(state.tokens, token.tokenId, token);
    // },
    setToken(state, token) {
        state.token = token;
    },
    setAllAccountsAndCurrentAccount(state, { data, email }) {
        for (const key in data) {
            const account = data[key];
            Vue.set(state.accounts, account.accountId, account);
            if (email === account.wallet && email != 'admin@gmail.com') {
                state.account = account;
            }
        }
    },
    clearHederaAccount(state) {
        state.admin = {};
        state.account = {};
        state.admin = {};
        state.token = {};
        state.tokens = {};
    }
}

const actions = {
    setAccounts({ commit, state }, accounts) {
        for (let account of accounts) {
            commit('setAccount', account)
        }
    },
    updateAccounts({commit}, email) {
        ref.child('account').once('value').then(accounts => {
            const data = accounts.val();
            commit('setAllAccountsAndCurrentAccount', {data, email})
        })
    },
    fetchAdmin({commit}, admin) {
        commit('setAdmin', admin);
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}