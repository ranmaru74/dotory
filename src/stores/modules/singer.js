

const state = {
    singer: {}
}

const mutations = {
    setSinger(state, singer) {
        state.singer = singer;
    }
}

const actions = {}

const getters = {
    selectedSinger: (state) => {
        return state.singer;
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}