import Vue from 'vue';
import Vuex from 'vuex';

import products from './modules/products';
import auth from './modules/auth';
import messages from './modules/messages';
import hederaAccount from './modules/hederaAccount';

import * as actions from './actions';
import singer from "@/stores/modules/singer";

Vue.use(Vuex);

export default new Vuex.Store({
    actions,
    modules: {
        auth,
        products,
        messages,
        hederaAccount,
        singer
    }
});